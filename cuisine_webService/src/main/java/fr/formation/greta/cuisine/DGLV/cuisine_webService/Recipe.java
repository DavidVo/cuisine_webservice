package fr.formation.greta.cuisine.DGLV.cuisine_webService;

public class Recipe {
	
	private String name;
	private int duration;
	private String text;
	private String img;
	
	public String getImg() {
		return img;
	}
	public void setImg(String img) {
		this.img = img;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getDuration() {
		return duration;
	}
	public void setDuration(int duration) {
		this.duration = duration;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	
	public Recipe(String name, int duration, String text, String img) {
		super();
		this.name = name;
		this.duration = duration;
		this.text = text;
		this.img = img;
	}
	
	public Recipe() {
		this("",0,"","");
	}
	
	@Override
	public String toString() {
		return "Recipes [name=" + name + ", duration=" + duration + ", text=" + text + "]";
	}
	
	

}
