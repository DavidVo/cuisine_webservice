package fr.formation.greta.cuisine.DGLV.cuisine_webService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CookBookController {
	
	// Ici on pourrait ajouter consumes = prend en entré, pour ajouter un format d'entré
	@RequestMapping(value="/complete", method=RequestMethod.GET, produces ="application/json")
	public boolean isComplete() {
		return false;
	}
	
	// Convention permettant d'afficher l'état du serveur 
	// Le GetMapping correspond a un requestmapping avec comme method Get
	@GetMapping(value="/health", produces ="application/json")
	public Map<String, String> getHealth() {
		
		Map<String, String> stateMap = new HashMap<String, String>();
		
		String cookBookState = "incomplete";
		String dataState = "Ok";
		String serverState = "Ok";
		
		stateMap.put("cookBook", cookBookState);
		stateMap.put("data", dataState);
		stateMap.put("server", serverState);
	
		return stateMap;
	}
	
	
	@GetMapping(value="/recipe", produces ="application/json")
	public List<Recipe> getRecipes(){
		List<Recipe> recipeList = new ArrayList<>();
		Recipe r1 = new Recipe("Choucroute", 2, "Pour faire une bonne choucroute il faut... ", "img/choucroute.jpg");
		Recipe r2 = new Recipe("Cassoulet", 4, "Pour faire un bonCassoulet il faut... ", "img/cassoulet.jpg");
		Recipe r3 = new Recipe("Poulet roti", 1, "Pour faire un bon Poulet roti il faut... ", "img/poulet.jpg");
		
		recipeList.add(r1);
		recipeList.add(r2);
		recipeList.add(r3);
		
		return recipeList;
	}
	
	@GetMapping(value="/complete.xml", produces="text/xml")
	public boolean isCompleteXML() {
		return isComplete();
	}
	
	@GetMapping(value="/recipe/{re}", produces="application/json")
	public Recipe getRecipe(@PathVariable("re") int n) {
		return getRecipes().get(n);
	}
	
	
	@DeleteMapping(value="/recipe/{de}")
	public void deleteRecipe(@PathVariable("de") int n) {
		System.out.println("Recipe deleted: " + n);
	}
	
	@PostMapping(value="/recipe")
	public void postRecipe(@RequestBody Recipe r) {
		
		getRecipes().add(r);
		
		
		System.out.println("A new recipe had been added");
	}
	

}
