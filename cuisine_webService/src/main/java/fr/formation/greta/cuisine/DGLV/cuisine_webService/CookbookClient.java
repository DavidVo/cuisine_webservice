package fr.formation.greta.cuisine.DGLV.cuisine_webService;

import java.util.Map;

import org.springframework.web.client.RestTemplate;

public class CookbookClient {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			RestTemplate rt = new RestTemplate();
			Boolean complete = rt.getForObject("http://localhost:8082/complete", Boolean.class);
			
			System.out.println("Show me if you\'re complete: " + complete);
			
			Recipe recipe2 = rt.getForObject("http://localhost:8082/recipe/1", Recipe.class);
			
			System.out.println("Show me a recipe: " + recipe2);
			
			System.out.println("WP" + rt.getForObject("https://fr.wikipedia.org/w/api.php?action=query&list=search&format=json&srsearch=cassoulet", Map.class)); 
			
			
			rt.delete("http://localhost:8082/recipe/1");
			
			rt.postForLocation("http://localhost:8082/recipe", new Recipe());

		} catch(Exception ex) { ex.printStackTrace();}
	}
}
